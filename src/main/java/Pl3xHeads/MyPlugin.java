package Pl3xHeads;

import java.io.File;
import java.util.List;

import net.pl3x.pl3xheads.commands.Pl3xHeads;
import net.pl3x.pl3xheads.tasks.StartMetrics;
import net.pl3x.pl3xlibs.Logger;
import net.pl3x.pl3xlibs.Material;
import net.pl3x.pl3xlibs.Pl3xLibs;
import net.pl3x.pl3xlibs.Skull;
import net.pl3x.pl3xlibs.configuration.BaseConfig;
import PluginReference.MC_DamageType;
import PluginReference.MC_Entity;
import PluginReference.MC_ItemStack;
import PluginReference.MC_Player;
import PluginReference.MC_Server;
import PluginReference.PluginBase;
import PluginReference.PluginInfo;

public class MyPlugin extends PluginBase {
	private static MyPlugin instance;
	private MC_Server server;
	private BaseConfig config = null;
	private Logger logger = null;

	public PluginInfo getPluginInfo() {
		PluginInfo info = new PluginInfo();
		info.name = "Pl3xHeads";
		info.version = "0.2";
		info.description = "Get the head of your victim!";
		info.optionalData.put("plugin-directory", "plugins_mod" + File.separator + info.name + File.separator);
		return info;
	}

	public static MyPlugin getInstance() {
		return instance;
	}

	@Override
	public void onStartup(MC_Server argServer) {
		server = argServer;
		instance = this;
		init();
		getServer().registerCommand(new Pl3xHeads(this));
		getLogger().info(getPluginInfo().name + " v" + getPluginInfo().version + " by BillyGalbreath is now enabled.");
	}

	@Override
	public void onShutdown() {
		disable();
		getLogger().info("Plugin disabled.");
	}

	private void init() {
		getConfig();
		Pl3xLibs.getScheduler().scheduleTask(getPluginInfo().name, new StartMetrics(this), 100);
	}

	private void disable() {
		config = null;
		Pl3xLibs.getScheduler().cancelAllRunnables(getPluginInfo().name);
	}

	public void reload() {
		disable();
		init();
	}

	public MC_Server getServer() {
		return server;
	}

	public BaseConfig getConfig() {
		if (config == null) {
			config = new BaseConfig(MyPlugin.class, getPluginInfo(), "", "config.ini");
			config.load();
		}
		return config;
	}

	public Logger getLogger() {
		if (logger == null) {
			logger = Pl3xLibs.getLogger(getPluginInfo());
		}
		return logger;
	}

	@Override
	public void onPlayerDeath(MC_Player plrVictim, MC_Player plrKiller, MC_DamageType dmgType, String deathMsg) {
		if (plrKiller == null) {
			return;
		}
		if (!plrKiller.hasPermission("pl3xheads.behead.player")) {
			return;
		}
		if (plrVictim.hasPermission("pl3xheads.nodrop")) {
			return;
		}
		if (!dmgType.equals(MC_DamageType.PLAYER)) {
			return;
		}
		if (!isValidWeapon(plrKiller.getItemInHand())) {
			return;
		}
		Skull skull = Pl3xLibs.getSkull(plrVictim);
		if (skull == null) {
			return;
		}
		if (!dropChance(skull)) {
			return;
		}
		skull.setSkin(plrVictim.getName());
		dropSkullInWorld(plrKiller, plrVictim, skull);
		deathMsg = getConfig().get("player-beheaded-broadcast", "").replace("{victim}", plrVictim.getCustomName()).replace("{killer}", plrKiller.getCustomName());
		getServer().broadcastMessage(Pl3xLibs.colorize(deathMsg));
	}

	@Override
	public void onNonPlayerEntityDeath(MC_Entity entVictim, MC_Entity entKiller, MC_DamageType dmgType) {
		if (entKiller == null) {
			return;
		}
		if (!(entKiller instanceof MC_Player)) {
			return;
		}
		MC_Player plrKiller = (MC_Player) entKiller;
		if (!plrKiller.hasPermission("pl3xheads.behead.mob")) {
			return;
		}
		if (!dmgType.equals(MC_DamageType.PLAYER)) {
			return;
		}
		if (!isValidWeapon(plrKiller.getItemInHand())) {
			return;
		}
		Skull skull = Pl3xLibs.getSkull(entVictim);
		if (skull == null) {
			return;
		}
		if (!dropChance(skull)) {
			return;
		}
		dropSkullInWorld(plrKiller, entVictim, skull);
		String deathMsg = getConfig().get("mob-beheaded-broadcast", "").replace("{mobtype}", skull.getName()).replace("{killer}", plrKiller.getCustomName());
		getServer().broadcastMessage(Pl3xLibs.colorize(deathMsg));
	}

	private boolean isValidWeapon(MC_ItemStack itemInHand) {
		List<String> weapons = getConfig().getStringList("weapons");
		if (weapons.isEmpty()) {
			// setup some defaults in case using old config.ini
			weapons.add("diamond_sword");
			weapons.add("diamond_axe");
			weapons.add("iron_sword");
			weapons.add("iron_axe");
		}
		int handId = itemInHand.getId();
		for (String weapon : weapons) {
			Material material = Material.matchMaterial(weapon);
			if (material != null && material.getId() == handId) {
				return true;
			}
		}
		return false;
	}

	private boolean dropChance(Skull skull) {
		double chance = getConfig().getDouble("chance-" + skull.getName(), 0D);
		if (chance <= 0.0) {
			return false; // disabled
		}
		if (chance > 100.0) {
			chance = 100.0;
		}
		double random = Math.random() * 100;
		if (random < chance) {
			return true; // passed
		}
		return false; // failed
	}

	private void dropSkullInWorld(MC_Player killer, MC_Entity victim, Skull skull) {
		String headLore = getConfig().get("head-lore", "&dThis head belongs to &7{killer}").replace("{killer}", killer.getCustomName()).replace("{victim}", victim.getCustomName());
		MC_ItemStack skullItem = Pl3xLibs.getSkullItem(skull, victim.getName(), headLore);
		Pl3xLibs.getWorld(victim.getLocation().dimension).dropItem(skullItem, victim.getLocation(), null);
	}
}
