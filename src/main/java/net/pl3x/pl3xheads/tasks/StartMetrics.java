package net.pl3x.pl3xheads.tasks;

import java.io.IOException;

import net.pl3x.pl3xlibs.MetricsLite;
import net.pl3x.pl3xlibs.scheduler.Pl3xRunnable;
import Pl3xHeads.MyPlugin;

public class StartMetrics extends Pl3xRunnable {
	private MyPlugin plugin;
	private MetricsLite metrics;

	public StartMetrics(MyPlugin plugin) {
		this.plugin = plugin;
	}

	@Override
	public void run() {
		plugin.getLogger().debug("Starting metrics!");
		try {
			metrics = new MetricsLite(plugin.getPluginInfo());
			metrics.start();
		} catch (IOException e) {
			plugin.getLogger().error("Failed to start Metrics: " + e.getMessage());
		}
	}
}
