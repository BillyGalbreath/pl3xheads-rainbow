package net.pl3x.pl3xheads.commands;

import java.util.Arrays;
import java.util.List;

import net.pl3x.pl3xlibs.Pl3xLibs;
import Pl3xHeads.MyPlugin;
import PluginReference.MC_Command;
import PluginReference.MC_Player;

public class Pl3xHeads implements MC_Command {
	private MyPlugin plugin;

	public Pl3xHeads(MyPlugin plugin) {
		this.plugin = plugin;
	}

	@Override
	public List<String> getAliases() {
		return Arrays.asList("head", "heads");
	}

	@Override
	public String getCommandName() {
		return "pl3xheads";
	}

	@Override
	public String getHelpLine(MC_Player player) {
		if (!hasPermissionToUse(player)) {
			return null;
		}
		if (player == null) {
			return "/pl3xheads (reload) - View version info or reload plugin";
		}
		return Pl3xLibs.colorize("&e/&7pl3xheads &e(&7reload&e) &e- &dView version info or reload plugin");
	}

	@Override
	public List<String> getTabCompletionList(MC_Player player, String[] args) {
		return null;
	}

	@Override
	public void handleCommand(MC_Player player, String[] args) {
		if (args.length > 0 && args[0].equalsIgnoreCase("reload")) {
			plugin.reload();
			Pl3xLibs.sendMessage(player, "&d" + plugin.getPluginInfo().name + " reloaded.");
			return;
		}
		Pl3xLibs.sendMessage(player, "&d" + plugin.getPluginInfo().name + " v&7" + plugin.getPluginInfo().version + "&d.");
	}

	@Override
	public boolean hasPermissionToUse(MC_Player player) {
		if (player == null) {
			return true; // ALLOW CONSOLE
		}
		return player.hasPermission("pl3xheads.command.pl3xheads");
	}
}
